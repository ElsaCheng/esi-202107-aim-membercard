"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var autoprefixerOptions = {};
var slim = require("gulp-slim");
var browserSync = require("browser-sync").create();
var newer = require("gulp-newer");
var gulpIgnore = require("gulp-ignore");

var paths = {
  html: {
    input: "src/slim/**/*.slim",
    output: "dist/",
  },
  styles: {
    input: "src/sass/**/*.scss",
    output: "dist/css/",
  },
};

// SASS to CSS
function style() {
  return gulp
    .src(paths.styles.input)
    .pipe(sourcemaps.init()) // 初始化 sourcemaps
    .pipe(
      sass({
        outputStyle: "compressed",
      }).on("error", sass.logError)
    )
    .pipe(autoprefixer())
    .pipe(sourcemaps.write("./")) // 生成 sourcemaps 文件 (.map)
    .pipe(gulp.dest(paths.styles.output))
    .pipe(browserSync.stream());
}

// SLIM to HTML (for all pages)

function htmlPage() {
  return gulp
    .src([paths.html.input, "!src/slim/includes/**"])
    .pipe(
      newer({
        dest: paths.html.output,
        ext: ".html",
      })
    )
    .pipe(
      slim({
        pretty: true,
        options: "encoding='utf-8'",
        require: "slim/include", // 呼叫include plug-in
        format: "xhtml",
        options: 'include_dirs=["src/slim/includes/"]',
      })
    )
    .pipe(gulp.dest(paths.html.output))
    .pipe(browserSync.stream());
}

// SLIM to HTML (for include)

function htmlInclude() {
  return gulp
    .src([paths.html.input, "!src/slim/includes/**"])
    .pipe(
      slim({
        pretty: true,
        options: "encoding='utf-8'",
        require: "slim/include", // 呼叫include plug-in
        format: "xhtml",
        options: 'include_dirs=["src/slim/includes/"]',
      })
    )
    .pipe(gulp.dest(paths.html.output))
    .pipe(browserSync.stream());
}

// BrowserSync Reload
function browserSyncReload() {
  browserSync.reload();
}

// Add browsersync initialization at the start of the watch task
function watch() {
  browserSync.init({
    // You can tell browserSync to use this directory and serve it as a mini-server
    server: {
      baseDir: "./dist",
      // index: "2.shop--index.html"
    },
  });
  gulp.watch(paths.styles.input, style);
  gulp.watch(paths.html.input, htmlPage);
  gulp.watch(["src/slim/includes/*.slim"], htmlInclude);
  gulp.watch(
    ["./dist/js/**/*", "./dist/images/**/*", "./dist/fonts/**/*"],
    browserSyncReload
  );
}

// export tasks
exports.style = style;
exports.htmlPage = htmlPage;
exports.htmlInclude = htmlInclude;
exports.watch = watch;
exports.default = watch;
